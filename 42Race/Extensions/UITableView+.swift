//
//  UITableView+.swift
//  42Race
//
//  Created by Duong Dinh on 3/16/22.
//

import UIKit

extension UITableView {
    
    func registerNibCell(_ cellType: UITableViewCell.Type) {
        register(cellType.nib, forCellReuseIdentifier: cellType.identifier)
    }
    
    func registerClassCell(_ cellType: UITableViewCell.Type) {
        register(cellType, forCellReuseIdentifier: cellType.identifier)
    }
    
    func dequeueCell<C: UITableViewCell>(_ cellType: C.Type) -> C {
        dequeueReusableCell(withIdentifier: cellType.identifier) as! C
    }
    
    func registerHeaderFooter(_ type: UITableViewHeaderFooterView.Type) {
        register(type.nib, forHeaderFooterViewReuseIdentifier: type.identifier)
    }
    
    func dequeueHeaderFooter<T: UITableViewHeaderFooterView>(_ type: T.Type) -> T {
        dequeueReusableHeaderFooterView(withIdentifier: T.identifier) as! T
    }
    
}

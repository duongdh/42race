//
//  UITableViewCell+.swift
//  42Race
//
//  Created by Duong Dinh on 3/16/22.
//

import UIKit

extension UITableViewCell {

    static let empty = UITableViewCell()
    
    class var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    class var identifier: String {
        return String(describing: self)
    }

    /// Search up the view hierarchy of the table view cell to find the containing table view
    var tableView: UITableView? {
        get {
            var supView: UIView? = superview
            while !(supView is UITableView) && supView != nil {
                supView = supView?.superview
            }
            
            return supView as? UITableView
        }
    }
    
}

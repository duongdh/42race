//
//  String+.swift
//  42Race
//
//  Created by Duong Dinh on 3/19/22.
//

import Foundation

extension String {
    
    func toDisplayTime() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HHmm"
        dateFormatter.timeZone = .current
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        
        guard let date = dateFormatter.date(from: self) else { return nil }
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.timeZone = .current
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        
        return dateFormatter.string(from: date)
    }
}

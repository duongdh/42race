//
//  UITableViewHeaderFooterView+.swift
//  42Race
//
//  Created by Duong Dinh on 3/16/22.
//

import UIKit

extension UITableViewHeaderFooterView {
    
    class var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    class var identifier: String {
        return String(describing: self)
    }
}

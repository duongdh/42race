//
//  Endpoint.swift
//  42Race
//
//  Created by Duong Dinh on 3/19/22.
//

import Foundation

class YelpFusionAPI {
    static let apiKey = "xZ6Io3_O6Ig6E2j8msQKfOEb0-vEZsDe2wo5OsMC11NCkKKu1YmNH_AWL6kR459umthaj858iRmoz2ARsHTXdwbhPjOfcwXa26fUgIO6VE624sS81UTF-DX-S0gwYnYx"
    static let baseURL = "https://api.yelp.com/v3"
    
    static let searchURL = baseURL + "/businesses/search"
    static func detailURL(id: String) -> String {
        return baseURL + "/businesses/\(id)"
    }
    static func reviewURL(id: String) -> String {
        return baseURL + "/businesses/\(id)/reviews"
    }
}

enum SortByOption: String {
    case distance = "distance"
    case rating = "rating"
}

enum SearchByOption: String {
    case businessName
    case location
    case cuisineType
}

protocol YelpEndpoint {
    var url: URL { get }
}

struct YelpSearchEndpoint: YelpEndpoint {
    let term: String
    let searchBy: SearchByOption
    let sortBy: SortByOption
    let coordinate: Coordinate
    
    var url: URL {
        let url = YelpFusionAPI.searchURL
        
        var urlComponents = URLComponents(string: url)!
        var queryItems = [URLQueryItem]()
        
        switch searchBy {
        case .location:
            let locationItem = URLQueryItem(name: "location", value: term)
            queryItems.append(locationItem)
        default:
            let termItem = URLQueryItem(name: "term", value: term)
            let longitude = URLQueryItem(name: "longitude", value: "\(coordinate.longitude)")
            let latitude = URLQueryItem(name: "latitude", value: "\(coordinate.latitude)")
            queryItems.append(termItem)
            queryItems.append(longitude)
            queryItems.append(latitude)
        }
        let locationItem = URLQueryItem(name: "sort_by", value: sortBy.rawValue)
        queryItems.append(locationItem)
        urlComponents.queryItems = queryItems
        
        return urlComponents.url!
    }
}

struct YelpDetailEndpoint: YelpEndpoint {
    let id: String
    
    var url: URL {
        let urlString = YelpFusionAPI.detailURL(id: id)
        return URL(string: urlString)!
    }
}

struct YelpReviewEndpoint: YelpEndpoint {
    let id: String
    
    var url: URL {
        let urlString = YelpFusionAPI.reviewURL(id: id)
        return URL(string: urlString)!
    }
}

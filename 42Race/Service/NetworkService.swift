//
//  NetworkService.swift
//  42Race
//
//  Created by Duong Dinh on 3/16/22.
//

import Foundation
import RxSwift

class NetworkService {
    static let shared = NetworkService()
    
    func search(term: String, searchBy: SearchByOption, sortBy: SortByOption, coordinate: Coordinate) -> Observable<[MinifiedBusiness]> {
        let endpoint = YelpSearchEndpoint(term: term, searchBy: searchBy, sortBy: sortBy, coordinate: coordinate)
        
        var request = URLRequest(url: endpoint.url)
        request.addValue("Bearer \(YelpFusionAPI.apiKey)", forHTTPHeaderField: "Authorization")
        
        return Observable.create { observer in
            
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    observer.onError(error)
                }
                
                if let data = data {
                    do {
                        if let JSONString = String(data: data, encoding: String.Encoding.utf8) {
                            print(JSONString)
                        }
                        let responseModel = try JSONDecoder().decode(SearchBusinessResponse.self, from: data)
                        observer.onNext(responseModel.businesses)
                    } catch let error {
                        observer.onError(error)
                    }
                }
            }.resume()
            return Disposables.create()
        }
    }
    
    func getBusinessDetail(id: String) -> Observable<Business> {
        let endpoint = YelpDetailEndpoint(id: id)
        var request = URLRequest(url: endpoint.url)
        request.addValue("Bearer \(YelpFusionAPI.apiKey)", forHTTPHeaderField: "Authorization")
        
        return Observable.create { observer in
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    observer.onError(error)
                }
                
                if let data = data {
                    if let JSONString = String(data: data, encoding: String.Encoding.utf8) {
                        print(JSONString)
                    }
                    do {
                        let model = try JSONDecoder().decode(Business.self, from: data)
                        observer.onNext(model)
                    } catch let error {
                        observer.onError(error)
                    }
                }
            }.resume()
            
            return Disposables.create()
        }
    }
    
    func getBusinessReviews(id: String) -> Observable<[Review]> {
        let endpoint = YelpReviewEndpoint(id: id)
        var request = URLRequest(url: endpoint.url)
        request.addValue("Bearer \(YelpFusionAPI.apiKey)", forHTTPHeaderField: "Authorization")
        
        return Observable.create { observer in
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    observer.onError(error)
                }
                
                if let data = data {
                    if let JSONString = String(data: data, encoding: String.Encoding.utf8) {
                        print(JSONString)
                    }
                    do {
                        let model = try JSONDecoder().decode(GetReviewsResponse.self, from: data)
                        observer.onNext(model.reviews)
                    } catch let error {
                        observer.onError(error)
                    }
                }
            }.resume()
            
            return Disposables.create()
        }
    }
}

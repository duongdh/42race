//
//  Restaurant.swift
//  42Race
//
//  Created by Duong Dinh on 3/16/22.
//

import Foundation

struct SearchBusinessResponse: Codable {
    let total: Int
    let businesses: [MinifiedBusiness]
}

struct Region: Codable {
    let center: Coordinate
}

struct MinifiedBusiness: Codable {
    let id, name, image_url: String
    let rating: Double
    let categories: [Category]
    let review_count: Int
    
    var categoriesString: String {
        return (categories.map { $0.title }).joined(separator: ", ")
    }
}

struct Business: Codable {
    let id, name, image_url, display_phone: String
    let review_count: Int
    let rating: Double
    let distance: Double?
    let categories: [Category]
    let location: Location
    let coordinates: Coordinate
    let hours: [HoursOfOperation]?
    let special_hours: [SpecialHour]?
    
    var categoriesString: String {
        return (categories.map { $0.title }).joined(separator: ", ")
    }
}

struct Category: Codable {
    let alias, title: String
}

struct Coordinate: Codable {
    let latitude, longitude: Double
}

struct Location: Codable {
    let address1, city, zip_code, country, state: String
    let display_address: [String]
    
    var fullAddress: String {
        return display_address.joined(separator: "\n")
    }
}

struct SpecialHour: Codable {
    let date, start, end: String
    let is_overnight: Bool
    
    var displayText: String {
        let startTime = start.toDisplayTime() ?? ""
        let endTime = end.toDisplayTime() ?? ""
        return "\(date) from \(startTime) to \(endTime)"
    }
}

enum Weekday: Int, Codable {
    case monday = 0, tuesday, wednesday, thursday, friday, saturday, sunday
    
    var text: String {
        switch self {
        case .monday:
            return "Mon"
        case .tuesday:
            return "Tue"
        case .wednesday:
            return "Wed"
        case .thursday:
            return "Thu"
        case .friday:
            return "Fri"
        case .saturday:
            return "Sat"
        case .sunday:
            return "Sun"
        }
    }
}

struct HoursOfOperation: Codable {
    let open: [OperationalHour]
    let is_open_now: Bool
}

struct OperationalHour: Codable {
    let start, end: String
    let day: Weekday
    let is_overnight: Bool
    
    var displayHour: String {
        let startTime = start.toDisplayTime() ?? ""
        let endTime = end.toDisplayTime() ?? ""
        if startTime == endTime {
            return "24/7"
        } else {
            return "\(start.toDisplayTime() ?? "") - \(end.toDisplayTime() ?? "")"
        }
    }
}

struct GetReviewsResponse: Codable {
    let reviews: [Review]
}

struct Review: Codable {
    let id, text: String
    let rating: Double
    let user: ReviewUser
}

struct ReviewUser: Codable {
    let id, profile_url, name: String
    let image_url: String?
}

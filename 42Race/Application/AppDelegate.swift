//
//  AppDelegate.swift
//  42Race
//
//  Created by Duong Dinh on 3/15/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = makeRootVC()
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func makeRootVC() -> UIViewController {
        let businessListVC = UIStoryboard(name: "Main", bundle: .main) .instantiateViewController(withIdentifier: "RestaurantListVC") as! RestaurantListVC
        let navigation = UINavigationController(rootViewController: businessListVC)
        let navigator = RestaurantListNavigator()
        let viewModel = RestaurantListVM(navigator: navigator)
        businessListVC.viewModel = viewModel
        navigator.navigationVC = navigation
        return navigation
    }
}


//
//  RestaurantListNavigator.swift
//  42Race
//
//  Created by Duong Dinh on 3/18/22.
//

import Foundation
import UIKit

class RestaurantListNavigator {
    
    var navigationVC: UINavigationController?
    
    func toRestaurantDetail(id: String) {
        let storyBoard = UIStoryboard(name: "Main", bundle: .main)
        let businessDetailVC = storyBoard.instantiateViewController(withIdentifier: "RestaurantDetailVC") as! RestaurantDetailVC
        let viewModel = RestaurantDetailVM(id: id)
        businessDetailVC.viewModel = viewModel
        navigationVC?.pushViewController(businessDetailVC, animated: true)
    }
}

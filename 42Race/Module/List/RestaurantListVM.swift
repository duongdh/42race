//
//  RestaurantListVM.swift
//  42Race
//
//  Created by Duong Dinh on 3/16/22.
//

import Foundation
import RxSwift
import RxCocoa
import RxRelay

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}

class RestaurantListVM: ViewModelType {
    
    let service = NetworkService.shared
    let navigator: RestaurantListNavigator
    
    init(navigator: RestaurantListNavigator) {
        self.navigator = navigator
    }
    
    struct Input {
        let searchTermTrigger: Driver<String>
        let searchByTrigger: Driver<SearchByOption>
        let sortByTrigger: Driver<SortByOption>
        let locationTrigger: Driver<Coordinate>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let businesses: Driver<[MinifiedBusiness]>
        let error: Driver<Error>
        let select: Driver<Void>
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let searchCombine = Driver.combineLatest(input.searchTermTrigger, input.searchByTrigger, input.sortByTrigger, input.locationTrigger)
        
        let businesses = searchCombine.flatMapLatest { term, searchBy, sortBy, coordinate in
            return self.service.search(term: term, searchBy: searchBy, sortBy: sortBy, coordinate: coordinate)
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
        }
        
        let select = input.selection.withLatestFrom(businesses) { indexPath, businesses in
            return businesses[indexPath.row].id
        }.do(onNext: navigator.toRestaurantDetail(id:))
        
        return Output(fetching: activityIndicator.asDriver(),
                      businesses: businesses,
                      error: errorTracker.asDriver(),
                      select: select.mapToVoid())
    }
}

//
//  RestaurantListCell.swift
//  42Race
//
//  Created by Duong Dinh on 3/16/22.
//

import UIKit

class RestaurantListCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var ratingImageView: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var reviewCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        restaurantImageView.layer.cornerRadius = 4
        restaurantImageView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(_ business: MinifiedBusiness) {
        restaurantNameLabel.text = business.name
        restaurantImageView.kf.setImage(with: URL(string: business.image_url))
        distanceLabel.text = business.categoriesString
        let ratingName = "yelp.star.\(business.rating.rounded())"
        ratingImageView.image = UIImage(named: ratingName)
        reviewCountLabel.text = "\(business.review_count) reviews"
    }
}

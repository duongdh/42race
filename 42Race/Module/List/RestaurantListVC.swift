//
//  ViewController.swift
//  42Race
//
//  Created by Duong Dinh on 3/15/22.
//

import UIKit
import RxSwift
import RxCocoa
import CoreLocation

class RestaurantListVC: BaseVC {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchModeButton: UIButton!
    
    // MARK: - Variables
    var viewModel: RestaurantListVM!
    private var searchByOption = BehaviorRelay<SearchByOption>(value: .businessName)
    private var sortByOption = BehaviorRelay<SortByOption>(value: .distance)
    private var coordinate = PublishSubject<Coordinate>()
    let locationManager = CLLocationManager()
}

// MARK: - Life Cycle
extension RestaurantListVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        setupBindings()
        setupLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
}

// MARK: - CLLocationManagerDelegate
extension RestaurantListVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let coordinate = locations.last?.coordinate else { return }
        let location = Coordinate(latitude: coordinate.latitude, longitude: coordinate.longitude)
        self.coordinate.onNext(location)
    }
}

extension RestaurantListVC {
    
    private func setupComponents() {
        tableView.registerNibCell(RestaurantListCell.self)
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        sortButton.setTitle("", for: .normal)
        
        // Buttons
        sortButton.rx.tap.subscribe(onNext: { [weak self] _ in
            self?.sortButtonTapped()
        }).disposed(by: bag)
        
        searchModeButton.rx.tap.subscribe(onNext: { [weak self] _ in
            self?.searchModeButtonTapped()
        }).disposed(by: bag)
    }
    
    private func setupBindings() {
        let locationTrigger = coordinate.asDriverOnErrorJustComplete()
        let searchTermTrigger = searchBar.rx.text.orEmpty
            .throttle(.milliseconds(200), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .asDriverOnErrorJustComplete()
        let searchByTrigger = searchByOption.asDriverOnErrorJustComplete()
        let sortByTrigger = sortByOption.asDriverOnErrorJustComplete()
        
        let input = RestaurantListVM.Input(searchTermTrigger: searchTermTrigger,
                                           searchByTrigger: searchByTrigger,
                                           sortByTrigger: sortByTrigger,
                                           locationTrigger: locationTrigger,
                                           selection: tableView.rx.itemSelected.asDriver())
        
        let output = viewModel.transform(input: input)
        
        output.businesses.drive(tableView.rx.items(cellIdentifier: RestaurantListCell.identifier, cellType: RestaurantListCell.self)) { row, data, cell in
            cell.configure(data)
        }.disposed(by: bag)
        
        output.fetching
            .drive(loadingIndicator.rx.isAnimating)
            .disposed(by: bag)
        
        output.error
            .drive(onNext: { [weak self] error in
                self?.showError(error.localizedDescription)
            })
            .disposed(by: bag)
        
        output.select
            .drive()
            .disposed(by: bag)
        
        tableView.rx.itemSelected.subscribe(onNext: { [weak self] indexPath in
            self?.tableView.deselectRow(at: indexPath, animated: true)
        }).disposed(by: bag)
    }
    
    private func setupLocation() {
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    @objc private func searchModeButtonTapped() {
        let alert = UIAlertController(title: "Search by", message: nil, preferredStyle: .actionSheet)
        let businessName = UIAlertAction(title: "Business name", style: .default) { [weak self] _ in
            self?.searchByOption.accept(.businessName)
        }
        let location = UIAlertAction(title: "Location", style: .default) { [weak self] _ in
            self?.searchByOption.accept(.location)
        }
        let cuisine = UIAlertAction(title: "Cuisine", style: .default) { [weak self] _ in
            self?.searchByOption.accept(.cuisineType)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(businessName)
        alert.addAction(location)
        alert.addAction(cuisine)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc private func sortButtonTapped() {
        let alert = UIAlertController(title: "Sort by", message: nil, preferredStyle: .actionSheet)
        let distance = UIAlertAction(title: "Distance", style: .default) { [weak self] _ in
            self?.sortByOption.accept(.distance)
        }
        let price = UIAlertAction(title: "Rating", style: .default) { [weak self] _ in
            self?.sortByOption.accept(.rating)
        }
        alert.addAction(distance)
        alert.addAction(price)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
}

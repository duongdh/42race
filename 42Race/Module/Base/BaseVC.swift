//
//  FPBaseViewController.swift
//  FilePlayer
//
//  Created by Duong Dinh on 4/6/20.
//  Copyright © 2020 Duong Dinh. All rights reserved.
//

import UIKit
import RxSwift

class BaseVC: UIViewController {
    
    /// Loading indicator
    lazy var loadingIndicator: UIActivityIndicatorView = {
        let indicatorView = UIActivityIndicatorView()
        indicatorView.color = .lightGray
        indicatorView.hidesWhenStopped = true
        indicatorView.stopAnimating()
        indicatorView.translatesAutoresizingMaskIntoConstraints = false
        
        return indicatorView
    }()
    
    /// Dispose bag
    var bag = DisposeBag()
    
    deinit {
        #if DEBUG
        print("\(self.classForCoder) is deinited")
        #endif
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        view.addSubview(loadingIndicator)
        loadingIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loadingIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loadingIndicator.widthAnchor.constraint(equalToConstant: 100).isActive = true
        loadingIndicator.heightAnchor.constraint(equalToConstant: 100).isActive = true
    }
    
    func showError(_ error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .cancel)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return [.portrait]
    }
}

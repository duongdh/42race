//
//  RestaurantDetailVC.swift
//  42Race
//
//  Created by Duong Dinh on 3/16/22.
//

import UIKit
import Kingfisher
import RxSwift
import RxCocoa
import RxDataSources
import SnapKit

class RestaurantDetailVC: BaseVC {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    var viewModel: RestaurantDetailVM!
}

// MARK: - Life Cycle
extension RestaurantDetailVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
        setupBindings()
    }
    
    private func setupComponents() {
        // Table view
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.registerNibCell(BusinessInfoCell.self)
        tableView.registerClassCell(ReviewCell.self)
        tableView.registerClassCell(HourCell.self)
        tableView.registerClassCell(DealCell.self)
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.estimatedSectionHeaderHeight = 10
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.sectionFooterHeight = .leastNormalMagnitude
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func setupBindings() {
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let input = RestaurantDetailVM.Input(trigger: viewWillAppear)
        
        let output = viewModel.transform(input: input)
        
        let datasource = RxTableViewSectionedAnimatedDataSource<RestaurantDetailVM.Section>(configureCell: { ds, tv, ip, item in
            
            switch item {
            case .headerInfo(let business):
                let cell = tv.dequeueCell(BusinessInfoCell.self)
                cell.configure(business: business)
                cell.selectionStyle = .none
                
                return cell
                
            case .reviews(let review):
                let cell = tv.dequeueReusableCell(withIdentifier: ReviewCell.identifier, for: ip) as! ReviewCell
                cell.configure(review)
                cell.selectionStyle = .none
                
                return cell
                
            case .hours(let hour):
                let cell = tv.dequeueReusableCell(withIdentifier: HourCell.identifier, for: ip) as! HourCell
                cell.configure(hour)
                cell.selectionStyle = .none
                return cell
                
            case .deals(let deal):
                let cell = tv.dequeueReusableCell(withIdentifier: DealCell.identifier, for: ip) as! DealCell
                cell.configure(deal: deal)
                cell.selectionStyle = .none
                return cell
            }
        }, titleForHeaderInSection: { dataSource, sectionIndex in
            return (dataSource[sectionIndex]).model.title
        })
        
        output.sections
            .asDriver(onErrorJustReturn: [])
            .drive(tableView.rx.items(dataSource: datasource))
            .disposed(by: bag)
        
        output.loading
            .drive(loadingIndicator.rx.isAnimating)
            .disposed(by: bag)
        
        output.error
            .drive(onNext: { [weak self] error in
                self?.showError(error.localizedDescription)
            })
            .disposed(by: bag)
    }
}

//
//  RestaurantDetailVM.swift
//  42Race
//
//  Created by Duong Dinh on 3/16/22.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class RestaurantDetailVM: ViewModelType {
    
    typealias Section = AnimatableSectionModel<SectionEnum, CellEnum>

    enum SectionEnum: String, CaseIterable, IdentifiableType {
        case headerInfo
        case hours
        case reviews
        case deals
        
        var identity: String { rawValue }
        
        var title: String? {
            switch self {
            case .headerInfo:
                return "Info"
            case .hours:
                return "Hours"
            case .reviews:
                return "Reviews"
            case .deals:
                return "Deals"
            }
        }
    }

    enum CellEnum: IdentifiableType, Equatable {
        static func == (lhs: CellEnum, rhs: CellEnum) -> Bool {
            return lhs.identity == rhs.identity
        }
        
        case headerInfo(Business)
        case hours(hour: OperationalHour)
        case reviews(reviews: Review)
        case deals(deal: SpecialHour)
        
        var identity: String {
            return "\(self)"
        }
    }
    
    let service = NetworkService.shared
    let id: String
    
    init(id: String) {
        self.id = id
    }
    
    struct Input {
        let trigger: Driver<Void>
    }
    
    struct Output {
        let loading: Driver<Bool>
        let sections: Driver<[Section]>
        let error: Driver<Error>
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let business = input.trigger.flatMapLatest {
            return self.service.getBusinessDetail(id: self.id)
            .trackActivity(activityIndicator)
            .trackError(errorTracker)
            .asDriverOnErrorJustComplete()
        }
        
        let reviews = input.trigger.flatMapLatest {
            return self.service.getBusinessReviews(id: self.id)
            .trackActivity(activityIndicator)
            .trackError(errorTracker)
            .asDriverOnErrorJustComplete()
        }.startWith([])
        
        let sections = Driver.combineLatest(business, reviews) { business, reviews -> [Section] in
            let hasHours = (business.hours?.first?.open ?? []).count > 0
            let hasReviews = reviews.count > 0
            let hasDeals = (business.special_hours ?? []).count > 0
            
            return [
                .init(model: .headerInfo, items: [
                    .headerInfo(business)
                ]),
                
                hasHours ? .init(model: .hours, items: (business.hours?.first?.open ?? []).map { .hours(hour: $0) }) : nil,
                hasReviews ? .init(model: .reviews, items: reviews.map { .reviews(reviews: $0) }) : nil,
                hasDeals ? .init(model: .deals, items: (business.special_hours ?? []).map { .deals(deal: $0) }) : nil
            ].compactMap { $0 }
        }
            
        return Output(loading: activityIndicator.asDriver(),
                      sections: sections,
                      error: errorTracker.asDriver())
    }
}

//
//  ReviewCell.swift
//  42Race
//
//  Created by Duong Dinh on 3/19/22.
//

import UIKit
import SnapKit

class ReviewCell: UITableViewCell {
    // MARK: - Views
    private lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        
        imageView.backgroundColor = .black
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        
        return label
    }()
    
    private lazy var ratingImageView: UIImageView = {
        let imageView = UIImageView()
        
        return imageView
    }()
    
    private lazy var contentLabel: UILabel = {
        let label = UILabel()
        
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none

        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setupLayout() {
        contentView.addSubview(avatarImageView)
        avatarImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(5)
            make.top.equalToSuperview().inset(10)
            make.width.height.equalTo(30)
        }
        
        contentView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.centerY.equalTo(avatarImageView)
            make.leading.equalTo(avatarImageView.snp.trailing).offset(5)
            make.trailing.equalToSuperview().inset(5)
        }
        
        contentView.addSubview(ratingImageView)
        ratingImageView.snp.makeConstraints { make in
            make.top.equalTo(avatarImageView.snp.bottom).offset(10)
            make.leading.equalTo(avatarImageView)
            make.width.equalTo(100)
            make.height.equalTo(20)
        }
        
        contentView.addSubview(contentLabel)
        contentLabel.snp.makeConstraints { make in
            make.top.equalTo(ratingImageView.snp.bottom).offset(10)
            make.leading.equalTo(avatarImageView)
            make.trailing.equalToSuperview().inset(5)
            make.bottom.equalToSuperview().inset(10).priority(999)
        }
    }
    
    func configure(_ review: Review) {
        avatarImageView.kf.setImage(with: URL(string: review.user.image_url ?? ""))
        nameLabel.text = review.user.name
        contentLabel.text = review.text
        let ratingName = "yelp.star.\(review.rating.rounded())"
        ratingImageView.image = UIImage(named: ratingName)
    }
}

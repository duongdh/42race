//
//  HourCell.swift
//  42Race
//
//  Created by Duong Dinh on 3/19/22.
//

import UIKit
import SnapKit

class HourCell: UITableViewCell {
    
    private lazy var weekDayLabel: UILabel = {
        let label = UILabel()
        
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        
        return label
    }()
    
    
    private lazy var timeLabel: UILabel = {
        let label = UILabel()
        
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        
        return label
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none

        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupLayout() {
        contentView.addSubview(weekDayLabel)
        weekDayLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(5)
            make.top.equalToSuperview().inset(10)
        }
        
        contentView.addSubview(timeLabel)
        timeLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(10)
            make.leading.equalTo(weekDayLabel.snp.trailing).offset(20)
            make.trailing.equalToSuperview().inset(5)
            make.bottom.equalToSuperview().inset(5).priority(999)
        }
    }
    
    func configure(_ hour: OperationalHour) {
        weekDayLabel.text = hour.day.text
        timeLabel.text = hour.displayHour
    }
}

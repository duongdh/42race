//
//  BusinessInfoCell.swift
//  42Race
//
//  Created by Duong Dinh on 3/19/22.
//

import UIKit
import Kingfisher

class BusinessInfoCell: UITableViewCell {
    // MARK: - Views
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var ratingImageView: UIImageView!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var linkButton: UIButton!
    @IBOutlet weak var reviewCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(business: Business) {
        restaurantNameLabel.text = business.name
        addressLabel.text = business.location.fullAddress
        restaurantImageView.kf.setImage(with: URL(string: business.image_url), placeholder: UIImage())
        categoriesLabel.text = "Categories: " + business.categoriesString
        let ratingName = "yelp.star.\(business.rating.rounded())"
        ratingImageView.image = UIImage(named: ratingName)
        reviewCountLabel.text = "\(business.review_count)"
        contactLabel.text = business.display_phone
    }
}

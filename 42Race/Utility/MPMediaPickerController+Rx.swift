//
//  MPMediaPickerController+Rx.swift
//  FilePlayer
//
//  Created by Duong Dinh on 30/12/2020.
//  Copyright © 2020 Duong Dinh. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MediaPlayer

extension MPMediaPickerController: HasDelegate {
    public typealias Delegate = MPMediaPickerControllerDelegate
}

private final class RxMPMediaPickerControllerDelegateProxy: DelegateProxy<MPMediaPickerController, MPMediaPickerControllerDelegate>, DelegateProxyType, MPMediaPickerControllerDelegate {
    
    weak private (set) var controller: MPMediaPickerController?
    
    init(controller: ParentObject) {
        self.controller = controller
        super.init(parentObject: controller, delegateProxy: RxMPMediaPickerControllerDelegateProxy.self)
    }
    
    static func registerKnownImplementations() {
        register { RxMPMediaPickerControllerDelegateProxy(controller: $0) }
    }
}

extension Reactive where Base: MPMediaPickerController {
    
    /// Delegate proxy for `MPMediaPickerController`.
    var delegate: DelegateProxy<MPMediaPickerController, MPMediaPickerControllerDelegate> {
        return RxMPMediaPickerControllerDelegateProxy.proxy(for: base)
    }
    
    /// Tells that user has selected one or more documents.
    var didPickMediaItems: Observable<[MPMediaItem]> {
        return delegate
            .methodInvoked(#selector(MPMediaPickerControllerDelegate.mediaPicker(_:didPickMediaItems:)))
            .map { ($0.last as? MPMediaItemCollection)?.items ?? [] }
    }
    
    /// Tells that user canceled the document picker.
    var documentPickerWasCancelled: Observable<Void> {
        return delegate.methodInvoked(#selector(MPMediaPickerControllerDelegate.mediaPickerDidCancel(_:))).mapToVoid()
    }
}
